.text
.global _my_adder_unsafe

_my_adder_unsafe: # A simple adder function implemented in assembly, using the CDECL calling convention
    addq %rdi, %rsi
    movq %rsi, %rax
    ret

use anyhow::Error;

fn main() -> Result<(), Error> {
    // Assemble the `add.s` file
    cc::Build::new().file("asm/x86/add.s").compile("add");

    // Rebuild if `add.s` changed
    println!("cargo:rerun-if-changed=add.s");
    Ok(())
}

extern "cdecl" {
    /// A symbol to link in an addition function implemented in assembly
    fn _my_adder_unsafe(a: u64, b: u64) -> u64;
}

/// An addition function implemented in assembly
#[inline(always)]
pub fn my_adder(a: u64, b: u64) -> u64 {
    // Safety: this function has no undefined behaviour regardless of inputs
    unsafe { _my_adder_unsafe(a, b) }
}

#[test]
fn it_works() {
    assert_eq!(my_adder(2, 2), 4);
}
